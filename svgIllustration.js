
function startDesc(iconID, descID) {
    document.getElementById('capabilitiesSVG').style.pointerEvents = 'none';
    icon = document.getElementById(iconID);
    iconOb = icon.getBoundingClientRect();
    desc = document.getElementById(descID);
    desc.style.left = iconOb.x + 'px';
    desc.style.top = iconOb.y + 'px';
    desc.style.borderRadius = iconOb.right - iconOb.left + 'px';
    desc.style.width = iconOb.right - iconOb.left + 'px';
    desc.style.height = iconOb.bottom - iconOb.top + 'px';
    desc.style.opacity = '1';

    function finalDesc(desc) {
        document.getElementById('popupOverlay').style.pointerEvents = 'auto';
        document.getElementById('popupOverlay').style.opacity = '1';
        desc.style.transition = '.8s';
        desc.style.top = '50%';
        desc.style.left = '50%';
        desc.style.width = '100%';        
        desc.style.transform = 'translate(-50%,-50%)';
        desc.style.height = '100%';
        desc.style.maxWidth = '640px';
        desc.style.maxHeight = '300px';



        /*
width: 100%;
    height: 100%;
    max-width: 640px;
    max-height: 480px;
    top: 50%;
    left: 50%;
    transform: translate(-50%,-50%);
        */
        desc.style.pointerEvents = 'auto';
    }
    setTimeout(function() {
        finalDesc(desc)
    }, 300);

}

function closeDesc(iconID, descID) {
    document.getElementById('popupOverlay').style.pointerEvents = 'none';
    document.getElementById('popupOverlay').style.opacity = '0';
    icon = document.getElementById(iconID);
    iconOb = icon.getBoundingClientRect();
    desc = document.getElementById(descID);
    desc.style.left = iconOb.x + 'px';
    desc.style.top = iconOb.y + 'px';
    desc.style.borderRadius = iconOb.right - iconOb.left + 'px';
    desc.style.width = iconOb.right - iconOb.left + 'px';
    desc.style.height = iconOb.bottom - iconOb.top + 'px';


    function originDesc(desc) {
        desc.style.transition = '0';
        desc.style.opacity = '0';
        desc.style.pointerEvents = 'none';
        document.getElementById('capabilitiesSVG').style.pointerEvents = 'auto';

    }

    setTimeout(function() {
        originDesc(desc)
    }, 800);

}

